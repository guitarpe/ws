<?php
require_once("_funcoes.php");

class WSSantosDummont
{

    private $sis_manutencao = false;
    private $var_sistema = '';
    private $autenticado = false;
    private $arr_sistemas = array('SantosDummont');

    /* function valida_ip_acesso($ip)
      {
      $valido = false;
      foreach ($this->ips_validos as $value) {
      if ($value == $ip) {
      $valido = true;
      break;
      }
      }
      return $valido;
      } */

    function valida_sistema_acesso($sitema)
    {
        $valido = false;
        foreach ($this->arr_sistemas as $value) {
            if ($value == $sitema) {
                $this->var_sistema = $value;
                $valido = true;
                break;
            }
        }
        return $valido;
    }

    function authHeader($header)
    {

        $usuario = (!isset($header->usuario)) ? '' : $header->usuario;
        $senha = (!isset($header->senha)) ? '' : $header->senha;
        $token = (!isset($header->token)) ? '' : $header->token;

        $sistema = (!isset($header->sistema)) ? '' : $header->sistema;
        $ip_acesso = $_SERVER['REMOTE_ADDR'];

        if (empty($sistema)) {
            $this->autenticado = false;
            throw new SOAPFault('Dados para autenticacao invalidos', 401);
        } else {

            //$r_ip = $this->valida_ip_acesso($ip_acesso);
            $r_sistema = $this->valida_sistema_acesso($sistema);
            $this->autenticado = true;

//            if (!$r_ip) {
//                $this->autenticado = false;
//                throw new SOAPFault('Acesso ao WS Restrito, IP Invalido - ' . $ip_acesso, 401);
//                exit;
//            }

            if (!$r_sistema) {
                $this->autenticado = false;
                throw new SOAPFault('Sistema nao validado', 401);
                exit;
            }
        }
    }

    public function chamaMetodos($param, $metodo)
    {
        if ($this->sis_manutencao) {
            $arr_ret = array('erro' => true, 'message' => 'Sistema em manutenção', 'code' => -2, 'list' => array(refresh => 'S'));
            return json_encode($arr_ret);
        } else {

            if ($this->autenticado) {
                $cls_funcoes = new Funcoes();
                $retorno = $cls_funcoes->recebe_chamada($metodo, $this->var_sistema, (array) $param);

                return json_encode($retorno);
            } else {
                throw new SOAPFault('Dados para autenticacao invalidos', 401);
            }
        }
    }
}

$options = array('uri' => 'SantosDummont', 'soap_version' => SOAP_1_2);
$server = new SoapServer(null, $options);
$server->setClass('WSSantosDummont');
$server->handle();

ini_set('soap.wsdl_cache_enabled',0);
ini_set('soap.wsdl_cache_ttl',0);

?>