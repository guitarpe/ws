<?php
$options = array(
    'location' => 'http://www.santosdumontcomp.com.br/ws/_service.php',
    'uri' => 'http://191.252.58.75',
    'trace' => 1,
    'version' => SOAP_1_1
);

$client = new SoapClient(NULL, $options);

$autenticacao = new stdClass();

$autenticacao->sistema = 'SantosDummont';
$autenticacao->ip = $_SERVER["REMOTE_ADDR"];

$header_params = new SoapVar($autenticacao, SOAP_ENC_OBJECT);
$header = new SoapHeader("WSSantosDummont", "authHeader", $header_params, false);

$client->__setSoapHeaders($header);

try {

    $parametros = [
        'I_TOKEN' => 'ADM707887',
        'I_CLI_ID' => 162
    ];

    $metodo = 'servicoSDDadosClientes';
    $retorno = json_decode($client->chamaMetodos($parametros, $metodo), true);

    echo '<pre>';
    print_r($retorno);
    echo '</pre>';
} catch (SoapFault $e) {
    print_r($e->getMessage());
//    $response = $client->__getLastResponse();
//    $response = str_replace("&#x1A", '', $response); ///My Invalid Symbol
//    $response = str_ireplace(array('SOAP-ENV:', 'SOAP:'), '', $response);
//    $response = simplexml_load_string($response);
//    print_r($response);
}