<?php

class Conexao extends PDO
{

    private $cursores = array();
    private $DB_HOST = "localhost";
    private $DB_NAME = "dbsantosd";
    private $DB_USER = "dbsantosd";
    private $DB_PASS = 'o0&%$fvC49*hg';
    private $DB_PORT = "3306";

    public function __construct()
    {
        $url = filter_input(INPUT_SERVER, 'HTTP_HOST');

        if ($url === "localhost") {
            $this->DB_HOST = '191.252.58.75';
            $this->DB_PORT = "12006";
        }

        $dns = "mysql:host=$this->DB_HOST; dbname=$this->DB_NAME; port=$this->DB_PORT; charset=utf8;";
        $options = [
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
        ];

        try {
            $conn = parent::__construct($dns, $this->DB_USER, $this->DB_PASS, $options);
        } catch (PDOException $e) {
            return $e->getMessage();
        } finally {
            $conn = null;
        }

        date_default_timezone_set('America/Sao_Paulo');
    }

    public function desconecta()
    {

    }

    public function __destruct()
    {
        $this->desconecta();
    }

    public function trataLog($param, $mascara)
    {
        $parm_ = array();

        foreach ($param as $key => $value) {

            $parm_[$key] = ((isset($mascara[$key])) && ($mascara[$key] == true)) ? '######' : $value;
        }

        return serialize($parm_);
    }

    public function existeAlias($alias)
    {

        $sql_text = $this->getSQL($alias);

        if ($sql_text === false) {
            $this->desconecta();
            trigger_error(htmlentities("Nao existe um SQL para o alias solicitado " . $alias), E_USER_ERROR);
            return false;
        } else {
            return true;
        }
    }

    public function select($alias, &$parm = array(), $mascara = array())
    {
        $parm_ = $this->trataLog($parm, $mascara);

        $sql_text = $this->getSQL($alias);

        if ($sql_text === false) {
            $this->desconecta();
            trigger_error(htmlentities("Nao existe um SQL para o alias solicitado " . $alias), E_USER_ERROR);
            return;
        }

        $sth = $this->prepare($sql_text['SQLP_FULL']);
        $this->cursores[$alias] = $sth;

        foreach ($parm as $key => $value) {
            $tipo = ( is_int($value) ) ? PDO::PARAM_INT : PDO::PARAM_STR;
            $sth->bindValue("$key", $value, $tipo);
        }

        $ok = $sth->execute();

        $this->log_registra($alias, $parm_, $ok);

        if ($ok) {
            return true;
        } else {
            return false;
        }
    }

    private function getSQL($alias)
    {
        $sql = "SELECT * FROM CTRA_SQL WHERE ALIAS=:ALIAS";

        $array = [
            'ALIAS' => $alias
        ];

        $sth = $this->prepare($sql);

        foreach ($array as $key => $value) {
            $tipo = ( is_int($value) ) ? PDO::PARAM_INT : PDO::PARAM_STR;

            $sth->bindValue("$key", $value, $tipo);
        }

        $ok = $sth->execute();

        if ($ok) {
            $ret = $sth->fetch(PDO::FETCH_ASSOC);
            $ret['SQLP_FULL'] = $ret['SQL1'] . $ret['SQL2'];
            return $ret;
        } else {
            return false;
        }
    }

    public function getDados($alias, $parm, $mascara)
    {
        $parm_ = $this->trataLog($parm, $mascara);

        $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $sql_text = $this->getSQL($alias);

        $sth = $this->prepare($sql_text['SQLP_FULL']);
        $this->cursores[$alias] = $sth;

        foreach ($parm as $key => $value) {
            $tipo = ( is_int($value) ) ? PDO::PARAM_INT : PDO::PARAM_STR;
            $sth->bindValue("$key", $value, $tipo);
        }

        $ok = $sth->execute();

        if($alias != 'LerNotificacoes'){
            $this->log_registra($alias, $parm_, $ok);
        }

        $rows = $sth->fetchAll(PDO::FETCH_ASSOC);

        return $rows;
    }

    public function getDadosProcedure($alias, $parametros, $mascara)
    {

        $parm_ = $this->trataLog($parametros, $mascara);

        $sql_text = $this->getSQL($alias);

        $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);

        try {
            $sth = $this->prepare($sql_text['SQLP_FULL']);
            $this->cursores[$alias] = $sth;

            foreach ($parametros as $key => $value) {
                $tipo = ( is_int($value) ) ? PDO::PARAM_INT : (is_string($value) ? PDO::PARAM_STR : (is_bool($value) ? PDO::PARAM_BOOL : PDO::PARAM_NULL));

                $sth->bindValue(":$key", $value, $tipo);
            }

            $sth->closeCursor();
            $ok = $sth->execute();

            $rows = $sth->fetchAll(PDO::FETCH_ASSOC);

            if($alias != 'LerNotificacoes'){
                $this->log_registra($alias, $parm_, $ok);
            }

            return $rows;
        } catch (PDOException $erro) {
            return $erro->getMessage();
        }
    }

    public function getErro($alias)
    {
        return $this->cursores[$alias]->errorInfo();
    }

    public function escapa_str($str)
    {
        if (get_magic_quotes_gpc()) {
            return str_replace("\\", "\\\\", addcslashes($str, "\0..\37")); // homolog
        } else {
            return addcslashes($str, "\0..\37"); // private
        }
    }

    public function log_registra($alias, $parm, $ok)
    {
        if ($ok === true) {
            $message_erro = 'SUCESSO';
            $nome_arq = 'log_exec_';
        } else {
            $arr_erro = $this->getErro($alias);
            $message_erro = $arr_erro['code'] . ' - ' . $arr_erro['message'];
            $nome_arq = 'log_erro_';
        }
        $log_txt = '';
        $_sql = trim($alias);
        $arquivo = __FILE__;
        $arquivo = str_replace("\\", "/", $arquivo);
        $arquivo = substr($arquivo, 0, strrpos($arquivo, "/") + 1) . "/conexao_log/";
        $arquivo_log = $arquivo . $nome_arq . date("Ymd") . ".log";
        $arq = @fopen($arquivo_log, "ab");
        $log_txt .= date("d/m/Y G:i:s") . "\t";
        $log_txt .= $_SERVER["SCRIPT_NAME"] . "\t";
        $log_txt .= $this->escapa_str($alias) . "\t";
        $log_txt .= $this->escapa_str($_sql) . "\t" . $parm . "\t";
        $log_txt .= $message_erro . "\r\n";
        @fwrite($arq, $log_txt);
        @fclose($arq);
    }

    public function rollback()
    {
        $this->rollback();
    }

    public function commit()
    {
        $this->commit();
    }
}
