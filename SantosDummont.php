<?php
require_once('Base.php');

define('PROCEDURE', 'PROCEDURE');
define('SQL', 'SQL');
define('INSERT', 'INSERT');
define('UPDATE', 'UPDATE');
define('DELETE', 'DELETE');
define('SQLT_CHR', 1);
define('SQLT_INT', 3);

class SantosDummont extends Base
{

    private $sis;

    public function __construct($sistema = 'SantosDummont', $usuario = 'sistema')
    {
        parent::__construct($sistema, $usuario);

        $this->sis = $sistema;
    }

    public function consultarSQLBase($alias, $parametros = [], $mascara = [], $tipoQuery = SQL)
    {
        $erro = false;
        $message = '';
        $data = array();
        $att_token = 'N';

        $result = $this->conexao->existeAlias($alias);

        if ($result != false) {

            switch ($tipoQuery) {
                case SQL:

                    $resultFull = $this->conexao->getDados($alias, $parametros, $mascara);

                    if (empty($resultFull)) {
                        $erro = true;
                        $message = '[' . $resultFull . '] - Registro Nao Encontrado';
                    } else {

                        if (isset($resultFull[0]['O_COD_RETORNO'])) {
                            if ($resultFull[0]['O_COD_RETORNO'] <> '0' && trim($resultFull[0]['O_COD_RETORNO']) <> '') {
                                $erro = true;
                                $message = '[' . $resultFull[0]['O_COD_RETORNO'] . ']-' . $resultFull[0]['O_DESC_CURTO'];
                                $att_token = $resultFull[0]['O_TOKEN_INVALIDO'];
                                $data = array();
                            } else {
                                $erro = true;
                                $data = $resultFull;
                            }
                        } else {
                            $erro = true;
                        }
                    }

                    break;
                case PROCEDURE:

                    $resultFull = $this->conexao->getDadosProcedure($alias, $parametros, $mascara);

                    if (empty($resultFull)) {
                        $erro = true;
                        $message = '[' . $alias . '] - ' . $resultFull;
                    } else {
                        if (isset($resultFull[0]['O_COD_RETORNO'])) {
                            if ($resultFull[0]['O_COD_RETORNO'] <> '0' && trim($resultFull[0]['O_COD_RETORNO']) <> '') {
                                $erro = true;
                                $message = '[' . $resultFull[0]['O_COD_RETORNO'] . ']-' . $resultFull[0]['O_DESC_CURTO'];
                                $att_token = $resultFull[0]['O_TOKEN_INVALIDO'];
                                $data = array();
                            } else {
                                $erro = true;
                                $data = $resultFull;
                            }
                        } else {
                            $erro = true;
                        }
                        $data = $resultFull;
                    }

                    break;
                case INSERT:
                case UPDATE:
                case DELETE:
                    $erro = false;
                    $message = 'Sucesso';
                    $this->conexao->commit();

                    break;
            }

            return array("erro" => $erro, "message" => $message, 'att_token' => $att_token, "list" => $data);
        }

        $this->conexao->rollback();
        $msgErro = $this->conexao->getErro($this->_alias);
        $message = "Falha na consulta " . $msgErro['message'];

        return array("erro" => true, "message" => $message, 'att_token' => $att_token, "list" => $data);
    }

    function servicoSDValidaLogin($dados)
    {
        $alias = 'ValidaLogin';

        $parametros = [
            'I_US_LOGIN' => $dados['I_US_LOGIN'],
            'I_US_SENHA' => $dados['I_US_SENHA']
        ];

        $tipos = [
            'I_US_LOGIN' => array('l' => 200, 't' => SQLT_CHR),
            'I_US_SENHA' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDExibirMenu($dados)
    {
        $alias = 'ExibirMenu';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDMenuSelecionado($dados)
    {
        $alias = 'MenuSelecionadoPRC';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_METODO' => $dados['I_METODO'],
            'I_CONTROLLER' => $dados['I_CONTROLLER']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_METODO' => array('l' => 200, 't' => SQLT_CHR),
            'I_CONTROLLER' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDListaCategorias($dados)
    {
        $alias = 'ListaCategorias';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_CATEGORIA' => $dados['I_CATEGORIA']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_CATEGORIA' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDListaCategoriasProdutos($dados)
    {
        $alias = 'CategoriasProdutos';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDListaCategoriasdoProdutos($dados)
    {
        $alias = 'ListaCategoriasdoProd';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_PRD_ID' => $dados['I_PRD_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDDadosCategoria($dados)
    {
        $alias = 'DadosCategoria';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_ID' => $dados['I_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDListaProdutos($dados)
    {
        $alias = 'ListaProdutos';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_ORDER' => $dados['I_ORDER'],
            'I_DIR' => $dados['I_DIR'],
            'I_START' => $dados['I_START'],
            'I_LENGTH' => $dados['I_LENGTH'],
            'I_NOME' => $dados['I_NOME'],
            'I_SKU' => $dados['I_SKU'],
            'I_CATNOME' => $dados['I_CATNOME'],
            'I_STATUS' => $dados['I_STATUS'],
            'I_ESTOQUE' => $dados['I_ESTOQUE']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_ORDER' => array('l' => 200, 't' => SQLT_CHR),
            'I_DIR' => array('l' => 200, 't' => SQLT_CHR),
            'I_START' => array('l' => 200, 't' => SQLT_CHR),
            'I_LENGTH' => array('l' => 200, 't' => SQLT_CHR),
            'I_NOME' => array('l' => 200, 't' => SQLT_CHR),
            'I_SKU' => array('l' => 200, 't' => SQLT_CHR),
            'I_CATNOME' => array('l' => 200, 't' => SQLT_CHR),
            'I_STATUS' => array('l' => 200, 't' => SQLT_CHR),
            'I_ESTOQUE' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDProdutoExisteSKU($dados)
    {
        $alias = 'ProdutoExisteSKU';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_PRD_SKU' => $dados['I_PRD_SKU']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_SKU' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDListaProdutosAPI($dados)
    {
        $alias = 'ListaProdutosAPI';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDListaProdutosAPICron($dados)
    {
        $alias = 'ListaProdutosAPICron';

        $parametros = [];

        $tipos = [];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDListaCategoriasBuscaCadastrados($dados)
    {
        $alias = 'ListaCategoriasBuscaCadastrados';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_ORDER' => $dados['I_ORDER'],
            'I_DIR' => $dados['I_DIR'],
            'I_START' => $dados['I_START'],
            'I_LENGTH' => $dados['I_LENGTH'],
            'I_NOME' => $dados['I_NOME'],
            'I_PAI' => $dados['I_PAI'],
            'I_STATUS' => $dados['I_STATUS']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_ORDER' => array('l' => 200, 't' => SQLT_CHR),
            'I_DIR' => array('l' => 200, 't' => SQLT_CHR),
            'I_START' => array('l' => 200, 't' => SQLT_CHR),
            'I_LENGTH' => array('l' => 200, 't' => SQLT_CHR),
            'I_NOME' => array('l' => 200, 't' => SQLT_CHR),
            'I_PAI' => array('l' => 200, 't' => SQLT_CHR),
            'I_STATUS' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDDadosProduto($dados)
    {
        $alias = 'DadosProduto';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_ID' => $dados['I_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDListaCategoriasInfo($dados)
    {
        $alias = 'ListaCategoriasInfo';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_PRD_ID' => $dados['I_PRD_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDFotosProduto($dados)
    {
        $alias = 'FotosProduto';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_ID' => $dados['I_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDImagemProduto($dados)
    {
        $alias = 'ImagemProduto';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_PRD_ID' => $dados['I_PRD_ID'],
            'I_IMG' => $dados['I_IMG']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_IMG' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDCadastrarEditarCategoria($dados)
    {
        $alias = 'CadastrarEditarCategoria';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_CAT_ID' => $dados['I_CAT_ID'],
            'I_CAT_NOME' => $dados['I_CAT_NOME'],
            'I_CAT_STATUS' => $dados['I_CAT_STATUS'],
            'I_CAT_PAI' => $dados['I_CAT_PAI'],
            'I_CAT_POSICAO' => $dados['I_CAT_POSICAO'],
            'I_CAT_NIVEL' => $dados['I_CAT_NIVEL'],
            'I_CAT_URL' => $dados['I_CAT_URL'],
            'I_CAT_IMAGEM' => $dados['I_CAT_IMAGEM']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_CAT_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_CAT_NOME' => array('l' => 200, 't' => SQLT_CHR),
            'I_CAT_STATUS' => array('l' => 200, 't' => SQLT_CHR),
            'I_CAT_PAI' => array('l' => 200, 't' => SQLT_CHR),
            'I_CAT_POSICAO' => array('l' => 200, 't' => SQLT_CHR),
            'I_CAT_NIVEL' => array('l' => 200, 't' => SQLT_CHR),
            'I_CAT_URL' => array('l' => 200, 't' => SQLT_CHR),
            'I_CAT_IMAGEM' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDAtivaInativaCategoria($dados)
    {
        $alias = 'AtivaInativaCategoria';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_CAT_ID' => $dados['I_CAT_ID'],
            'I_CAT_STATUS' => $dados['I_CAT_STATUS']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_CAT_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_CAT_STATUS' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDExcluirCategoria($dados)
    {
        $alias = 'ExcluirCategoria';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_ID' => $dados['I_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDExcluirImagemCategoria($dados)
    {
        $alias = 'ExcluirImagemCategoria';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_ID' => $dados['I_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDAtualizaAPIProduto($dados)
    {
        $alias = 'AtualizaAPIProduto';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_PRD_SKU' => $dados['I_PRD_SKU'],
            'I_PRD_UNIDADE' => $dados['I_PRD_UNIDADE'],
            'I_PRD_NOME' => $dados['I_PRD_NOME'],
            'I_PRD_URL' => $dados['I_PRD_URL'],
            'I_PRD_QTDE_ESTOQUE' => $dados['I_PRD_QTDE_ESTOQUE'],
            'I_PRD_PRECO' => (string) $dados['I_PRD_PRECO'],
            'I_PRD_COD_CETRUS' => $dados['I_PRD_COD_CETRUS'],
            'I_PRD_MARCA' => $dados['I_PRD_MARCA'],
            'I_PRD_MARCA_URL' => $dados['I_PRD_MARCA_URL'],
            'I_PRD_PESO' => $dados['I_PRD_PESO'],
            'I_PRD_ALTURA' => $dados['I_PRD_ALTURA'],
            'I_PRD_LARGURA' => $dados['I_PRD_LARGURA'],
            'I_PRD_STATUS' => $dados['I_PRD_STATUS'],
            'I_PRD_COMPRIMENTO' => $dados['I_PRD_COMPRIMENTO'],
            'I_PRD_PROFUNDIDADE' => $dados['I_PRD_PROFUNDIDADE']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_SKU' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_UNIDADE' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_NOME' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_URL' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_QTDE_ESTOQUE' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_PRECO' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_COD_CETRUS' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_MARCA' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_MARCA_URL' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_PESO' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_ALTURA' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_LARGURA' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_STATUS' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_COMPRIMENTO' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_PROFUNDIDADE' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDAtualizaAPIProdutoCron($dados)
    {
        $alias = 'AtualizaAPIProdutoCron';

        $parametros = [
            'I_PRD_SKU' => $dados['I_PRD_SKU'],
            'I_PRD_UNIDADE' => $dados['I_PRD_UNIDADE'],
            'I_PRD_NOME' => $dados['I_PRD_NOME'],
            'I_PRD_URL' => $dados['I_PRD_URL'],
            'I_PRD_QTDE_ESTOQUE' => $dados['I_PRD_QTDE_ESTOQUE'],
            'I_PRD_PRECO' => (string) $dados['I_PRD_PRECO'],
            'I_PRD_COD_CETRUS' => $dados['I_PRD_COD_CETRUS'],
            'I_PRD_MARCA' => $dados['I_PRD_MARCA'],
            'I_PRD_MARCA_URL' => $dados['I_PRD_MARCA_URL'],
            'I_PRD_PESO' => $dados['I_PRD_PESO'],
            'I_PRD_ALTURA' => $dados['I_PRD_ALTURA'],
            'I_PRD_LARGURA' => $dados['I_PRD_LARGURA'],
            'I_PRD_STATUS' => $dados['I_PRD_STATUS'],
            'I_PRD_COMPRIMENTO' => $dados['I_PRD_COMPRIMENTO'],
            'I_PRD_PROFUNDIDADE' => $dados['I_PRD_PROFUNDIDADE']
        ];

        $tipos = [
            'I_PRD_SKU' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_UNIDADE' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_NOME' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_URL' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_QTDE_ESTOQUE' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_PRECO' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_COD_CETRUS' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_MARCA' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_MARCA_URL' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_PESO' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_ALTURA' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_LARGURA' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_STATUS' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_COMPRIMENTO' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_PROFUNDIDADE' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDAtivaInativaProduto($dados)
    {
        $alias = 'AtivaInativaProduto';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_PRD_ID' => $dados['I_PRD_ID'],
            'I_PRD_STATUS' => $dados['I_PRD_STATUS']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_STATUS' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDCadastrarEditarProduto($dados)
    {
        $alias = 'CadastrarEditarProduto';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_TIPO' => $dados['I_TIPO'],
            'I_PRD_ID' => $dados['I_PRD_ID'],
            'I_PRD_DESTAQUE' => $dados['I_PRD_DESTAQUE'],
            'I_CATEGORIAS' => $dados['I_CATEGORIAS'],
            'I_PRD_NOME' => $dados['I_PRD_NOME'],
            'I_PRD_PATH' => $dados['I_PRD_PATH'],
            'I_PRD_STATUS' => $dados['I_PRD_STATUS'],
            'I_PRD_QTDE_ESTOQUE' => $dados['I_PRD_QTDE_ESTOQUE'],
            'I_PRD_QTDE_MINIMA' => $dados['I_PRD_QTDE_MINIMA'],
            'I_PRD_SKU' => $dados['I_PRD_SKU'],
            'I_PRD_UNIDADE' => $dados['I_PRD_UNIDADE'],
            'I_PRD_PRECO' => (string) $dados['I_PRD_PRECO'],
            'I_PRD_PRECO_ANTERIOR' => (string) $dados['I_PRD_PRECO_ANTERIOR'],
            'I_PRD_TAXA' => (string) $dados['I_PRD_TAXA'],
            'I_PRD_MODELO' => $dados['I_PRD_MODELO'],
            'I_PRD_NOME_TECNICO' => $dados['I_PRD_NOME_TECNICO'],
            'I_PRD_FICHA_TECNICA' => $dados['I_PRD_FICHA_TECNICA'],
            'I_PRD_DESCRICAO_MINIMA' => $dados['I_PRD_DESCRICAO_MINIMA'],
            'I_PRD_MARCA' => $dados['I_PRD_MARCA'],
            'I_PRD_PESO' => $dados['I_PRD_PESO'],
            'I_PRD_LARGURA' => $dados['I_PRD_LARGURA'],
            'I_PRD_ALTURA' => $dados['I_PRD_ALTURA'],
            'I_PRD_COMPRIMENTO' => $dados['I_PRD_COMPRIMENTO'],
            'I_PRD_PROFUNDIDADE' => $dados['I_PRD_PROFUNDIDADE'],
            'I_PRD_VOLTAGEM' => $dados['I_PRD_VOLTAGEM'],
            'I_PRD_COR' => $dados['I_PRD_COR'],
            'I_PRD_TAGS' => $dados['I_PRD_TAGS'],
            'I_PAR_ID' => $dados['I_PAR_ID'],
            'I_PASTA_IMAGENS' => $dados['I_PASTA_IMAGENS'],
            'I_IMAGENS' => $dados['I_IMAGENS']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_TIPO' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_DESTAQUE' => array('l' => 200, 't' => SQLT_CHR),
            'I_CATEGORIAS' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_NOME' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_PATH' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_STATUS' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_QTDE_ESTOQUE' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_QTDE_MINIMA' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_SKU' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_UNIDADE' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_PRECO' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_PRECO_ANTERIOR' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_TAXA' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_MODELO' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_NOME_TECNICO' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_FICHA_TECNICA' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_DESCRICAO_MINIMA' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_MARCA' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_PESO' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_LARGURA' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_ALTURA' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_COMPRIMENTO' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_PROFUNDIDADE' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_VOLTAGEM' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_COR' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_TAGS' => array('l' => 200, 't' => SQLT_CHR),
            'I_PAR_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_PASTA_IMAGENS' => array('l' => 200, 't' => SQLT_CHR),
            'I_IMAGENS' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDExcluirProduto($dados)
    {
        $alias = 'ExcluirProduto';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_ID' => $dados['I_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDExcluirImagem($dados)
    {
        $alias = 'ExcluirImagem';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_PRD_ID' => $dados['I_PRD_ID'],
            'I_IMG' => $dados['I_IMG']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDSetarImagemPrincipal($dados)
    {
        $alias = 'SetarImagemPrincipal';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_PRD_ID' => $dados['I_PRD_ID'],
            'I_IMG' => $dados['I_IMG']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_IMG' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDListaFabricantes($dados)
    {

        $alias = 'ListaFabricantes';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDDadosFabricante($dados)
    {
        $alias = 'DadosFabricante';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_PAR_ID' => $dados['I_PAR_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_PAR_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDCadastrarEditarFabricante($dados)
    {

        $alias = 'CadastrarEditarFabricante';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_PAR_ID' => $dados['I_PAR_ID'],
            'I_PAR_NOME' => $dados['I_PAR_NOME'],
            'I_PAR_URL' => $dados['I_PAR_URL'],
            'I_PAR_STATUS' => $dados['I_PAR_STATUS'],
            'I_PAR_DESCRICAO' => $dados['I_PAR_DESCRICAO'],
            'I_PAR_INFO' => $dados['I_PAR_INFO'],
            'I_PAR_EXIBIR_SITE' => $dados['I_PAR_EXIBIR_SITE'],
            'I_PASTA_IMAGENS' => $dados['I_PASTA_IMAGENS'],
            'I_IMAGEM' => $dados['I_IMAGEM']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_PAR_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_PAR_NOME' => array('l' => 200, 't' => SQLT_CHR),
            'I_PAR_URL' => array('l' => 200, 't' => SQLT_CHR),
            'I_PAR_STATUS' => array('l' => 200, 't' => SQLT_CHR),
            'I_PAR_DESCRICAO' => array('l' => 200, 't' => SQLT_CHR),
            'I_PAR_INFO' => array('l' => 2000, 't' => SQLT_CHR),
            'I_PAR_EXIBIR_SITE' => array('l' => 2000, 't' => SQLT_CHR),
            'I_PASTA_IMAGENS' => array('l' => 200, 't' => SQLT_CHR),
            'I_IMAGEM' => array('l' => 200, 't' => SQLT_CHR),
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDAtivaInativaFabricante($dados)
    {
        $alias = 'AtivaInativaFabricante';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_PAR_ID' => $dados['I_PAR_ID'],
            'I_PAR_STATUS' => $dados['I_PAR_STATUS']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_PAR_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_PAR_STATUS' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDExcluirFabricante($dados)
    {

        $alias = 'ExcluirFabricante';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_PAR_ID' => $dados['I_PAR_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_PAR_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDListaCupons($dados)
    {

        $alias = 'ListaCupons';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDDadosCupon($dados)
    {
        $alias = 'DadosCupon';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_CUP_ID' => $dados['I_CUP_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_CUP_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDCadastrarEditarCupon($dados)
    {
        $alias = 'CadastrarEditarCupon';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_CUP_ID' => $dados['I_CUP_ID'],
            'I_CUP_TOKEN' => $dados['I_CUP_TOKEN'],
            'I_DT_INI' => $dados['I_DT_INI'],
            'I_DT_FIM' => $dados['I_DT_FIM'],
            'I_PRD_ID' => $dados['I_PRD_ID'],
            'I_CUP_DESCONTO' => $dados['I_CUP_DESCONTO'],
            'I_CUP_TIPO' => $dados['I_CUP_TIPO'],
            'I_CUP_STATUS' => $dados['I_CUP_STATUS'],
            'I_CUP_DESCRICAO' => $dados['I_CUP_DESCRICAO']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_CUP_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_CUP_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_DT_FIM' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_CUP_DESCONTO' => array('l' => 200, 't' => SQLT_CHR),
            'I_PAR_INFO' => array('l' => 2000, 't' => SQLT_CHR),
            'I_CUP_TIPO' => array('l' => 2000, 't' => SQLT_CHR),
            'I_CUP_STATUS' => array('l' => 200, 't' => SQLT_CHR),
            'I_CUP_DESCRICAO' => array('l' => 200, 't' => SQLT_CHR),
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDExcluirCupon($dados)
    {

        $alias = 'ExcluirCupon';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_CUP_ID' => $dados['I_CUP_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_CUP_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDAtivaInativaCupon($dados)
    {

        $alias = 'AtivaInativaCupon';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_CUP_ID' => $dados['I_CUP_ID'],
            'I_CUP_STATUS' => $dados['I_CUP_STATUS']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_CUP_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_CUP_STATUS' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDListaMenusCadastrados($dados)
    {

        $alias = 'MenusCadastrados';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDDadosMenu($dados)
    {

        $alias = 'DadosMenu';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_MN_ID' => $dados['I_MN_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_MN_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDCadastrarEditarMenus($dados)
    {

        $alias = 'CadastrarEditarMenus';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_MN_ID' => $dados['I_MN_ID'],
            'I_MN_NOME' => $dados['I_MN_NOME'],
            'I_MN_DESC' => $dados['I_MN_DESC'],
            'I_MN_LINK' => $dados['I_MN_LINK'],
            'I_MN_PAI' => $dados['I_MN_PAI'],
            'I_MN_IMG' => $dados['I_MN_IMG']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_MN_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_MN_NOME' => array('l' => 200, 't' => SQLT_CHR),
            'I_MN_DESC' => array('l' => 200, 't' => SQLT_CHR),
            'I_MN_LINK' => array('l' => 2000, 't' => SQLT_CHR),
            'I_MN_PAI' => array('l' => 200, 't' => SQLT_CHR),
            'I_MN_IMG' => array('l' => 200, 't' => SQLT_CHR),
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDExcluirMenu($dados)
    {

        $alias = 'ExcluirMenu';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_MN_ID' => $dados['I_MN_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_MN_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDListaRedesCadastrados($dados)
    {
        $alias = 'RedesCadastrados';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDDadosRede($dados)
    {

        $alias = 'DadosRede';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_RS_ID' => $dados['I_RS_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_RS_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDCadastrarEditarRedes($dados)
    {

        $alias = 'CadastrarEditarRedes';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_RS_ID' => $dados['I_RS_ID'],
            'I_RS_NOME' => $dados['I_RS_NOME'],
            'I_RS_DESC' => $dados['I_RS_DESC'],
            'I_RS_LINK' => $dados['I_RS_LINK'],
            'I_RS_ICO' => $dados['I_RS_ICO']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_RS_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_RS_NOME' => array('l' => 200, 't' => SQLT_CHR),
            'I_RS_DESC' => array('l' => 200, 't' => SQLT_CHR),
            'I_RS_LINK' => array('l' => 2000, 't' => SQLT_CHR),
            'I_RS_ICO' => array('l' => 200, 't' => SQLT_CHR),
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDExcluirRede($dados)
    {

        $alias = 'ExcluirRede';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_RS_ID' => $dados['I_RS_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_RS_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDListaBanners($dados)
    {
        $alias = 'ListaBanners';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDDadosBanner($dados)
    {
        $alias = 'DadosBanner';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_BAN_ID' => $dados['I_BAN_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_BAN_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDEditarConfigBanners($dados)
    {
        $alias = 'EditarConfigBanners';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_ID_CONFIG' => $dados['I_ID_CONFIG'],
            'I_BANNER_SENTIDO' => $dados['I_BANNER_SENTIDO'],
            'I_BANNER_TEMPO' => $dados['I_BANNER_TEMPO']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_ID_CONFIG' => array('l' => 200, 't' => SQLT_CHR),
            'I_BANNER_SENTIDO' => array('l' => 200, 't' => SQLT_CHR),
            'I_BANNER_TEMPO' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDCadastrarEditarBanner($dados)
    {
        $alias = 'CadastrarEditarBanner';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_BAN_ID' => $dados['I_BAN_ID'],
            'I_BAN_DESCRICAO' => $dados['I_BAN_DESCRICAO'],
            'I_LINK' => $dados['I_LINK'],
            'I_LOCAL_LINK' => $dados['I_LOCAL_LINK'],
            'I_BAN_POS_TEXTOS' => $dados['I_BAN_POS_TEXTOS'],
            'I_BAN_TEXTO_PRINCIPAL' => $dados['I_BAN_TEXTO_PRINCIPAL'],
            'I_BAN_EFEITO_TXT_PRINC' => $dados['I_BAN_EFEITO_TXT_PRINC'],
            'I_BAN_COR_TXT_PRI' => $dados['I_BAN_COR_TXT_PRI'],
            'I_BAN_SEG_TEXTO' => $dados['I_BAN_SEG_TEXTO'],
            'I_BAN_EFEITO_SEG_TEXTO' => $dados['I_BAN_EFEITO_SEG_TEXTO'],
            'I_BAN_COR_TXT_SEC' => $dados['I_BAN_COR_TXT_SEC'],
            'I_BAN_TER_TEXTO' => $dados['I_BAN_TER_TEXTO'],
            'I_BAN_EFEITO_TER_TEXTO' => $dados['I_BAN_EFEITO_TER_TEXTO'],
            'I_BAN_COR_TXT_TER' => $dados['I_BAN_COR_TXT_TER'],
            'I_BAN_BT_TEXTO' => $dados['I_BAN_BT_TEXTO'],
            'I_BAN_BT_COR' => $dados['I_BAN_BT_COR'],
            'I_BAN_STATUS' => $dados['I_BAN_STATUS'],
            'I_IMAGEM' => $dados['I_IMAGEM'],
            'I_PASTA_IMAGENS' => $dados['I_PASTA_IMAGENS']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_BAN_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_BAN_DESCRICAO' => array('l' => 200, 't' => SQLT_CHR),
            'I_LINK' => array('l' => 200, 't' => SQLT_CHR),
            'I_LOCAL_LINK' => array('l' => 200, 't' => SQLT_CHR),
            'I_BAN_POS_TEXTOS' => array('l' => 200, 't' => SQLT_CHR),
            'I_BAN_TEXTO_PRINCIPAL' => array('l' => 200, 't' => SQLT_CHR),
            'I_BAN_EFEITO_TXT_PRINC' => array('l' => 200, 't' => SQLT_CHR),
            'I_BAN_COR_TXT_PRI' => array('l' => 200, 't' => SQLT_CHR),
            'I_BAN_SEG_TEXTO' => array('l' => 200, 't' => SQLT_CHR),
            'I_BAN_EFEITO_SEG_TEXTO' => array('l' => 200, 't' => SQLT_CHR),
            'I_BAN_COR_TXT_SEC' => array('l' => 200, 't' => SQLT_CHR),
            'I_BAN_TER_TEXTO' => array('l' => 200, 't' => SQLT_CHR),
            'I_BAN_EFEITO_TER_TEXTO' => array('l' => 200, 't' => SQLT_CHR),
            'I_BAN_COR_TXT_TER' => array('l' => 200, 't' => SQLT_CHR),
            'I_BAN_BT_TEXTO' => array('l' => 200, 't' => SQLT_CHR),
            'I_BAN_BT_COR' => array('l' => 200, 't' => SQLT_CHR),
            'I_BAN_STATUS' => array('l' => 200, 't' => SQLT_CHR),
            'I_IMAGEM' => array('l' => 200, 't' => SQLT_CHR),
            'I_PASTA_IMAGENS' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDExcluirBanner($dados)
    {
        $alias = 'ExcluirBanner';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_BAN_ID' => $dados['I_BAN_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_BAN_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDAtivarInativarBanner($dados)
    {
        $alias = 'AtivarInativarBanner';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_BAN_ID' => $dados['I_BAN_ID'],
            'I_BAN_STATUS' => $dados['I_BAN_STATUS']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_BAN_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_BAN_STATUS' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDListaDestaquesCadastrados($dados)
    {
        $alias = 'ListaDestaques';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDDadosDestaque($dados)
    {
        $alias = 'DadosDestaque';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_DES_ID' => $dados['I_DES_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_DES_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDCadastrarEditarDestaque($dados)
    {
        $alias = 'CadastrarEditarDestaque';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_DES_ID' => $dados['I_DES_ID'],
            'I_DES_DESCRICAO' => $dados['I_DES_DESCRICAO'],
            'I_DES_TIPO' => $dados['I_DES_TIPO'],
            'I_DES_DATA_INI' => $dados['I_DES_DATA_INI'],
            'I_DES_DATA_FIM' => $dados['I_DES_DATA_FIM'],
            'I_DES_STATUS' => $dados['I_DES_STATUS'],
            'I_DES_SELECIONADOS' => $dados['I_DES_SELECIONADOS']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_DES_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_DES_DESCRICAO' => array('l' => 200, 't' => SQLT_CHR),
            'I_DES_TIPO' => array('l' => 200, 't' => SQLT_CHR),
            'I_DES_DATA_INI' => array('l' => 200, 't' => SQLT_CHR),
            'I_DES_DATA_FIM' => array('l' => 200, 't' => SQLT_CHR),
            'I_DES_STATUS' => array('l' => 200, 't' => SQLT_CHR),
            'I_DES_SELECIONADOS' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDExcluirDestaque($dados)
    {
        $alias = 'ExcluirDestaque';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_DES_ID' => $dados['I_DES_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_DES_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDPesquisaLista($dados)
    {
        $alias = 'PesquisaLista';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_TIPO' => $dados['I_TIPO'],
            'I_INFO' => $dados['I_INFO']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_TIPO' => array('l' => 200, 't' => SQLT_CHR),
            'I_INFO' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDPesquisaEsp($dados)
    {
        $alias = 'PesquisaLista';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_TIPO' => $dados['I_TIPO'],
            'I_INFO' => $dados['I_INFO']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_TIPO' => array('l' => 200, 't' => SQLT_CHR),
            'I_INFO' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDListaProdutosDestaque($dados)
    {
        $alias = 'ListaProdutosDestaque';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_DES_ID' => $dados['I_DES_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_DES_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDListaNovidadesCadastrados($dados)
    {
        $alias = 'ListaNovidades';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDDadosNovidade($dados)
    {
        $alias = 'DadosNovidade';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_NOV_ID' => $dados['I_NOV_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_NOV_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDCadastrarEditarNovidade($dados)
    {
        $alias = 'CadastrarEditarNovidade';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_NOV_ID' => $dados['I_NOV_ID'],
            'I_NOV_DESCRICAO' => $dados['I_NOV_DESCRICAO'],
            'I_NOV_TIPO' => $dados['I_NOV_TIPO'],
            'I_NOV_DATA_INI' => $dados['I_NOV_DATA_INI'],
            'I_NOV_DATA_FIM' => $dados['I_NOV_DATA_FIM'],
            'I_NOV_STATUS' => $dados['I_NOV_STATUS'],
            'I_NOV_SELECIONADOS' => $dados['I_NOV_SELECIONADOS']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_NOV_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_NOV_DESCRICAO' => array('l' => 200, 't' => SQLT_CHR),
            'I_NOV_TIPO' => array('l' => 200, 't' => SQLT_CHR),
            'I_NOV_DATA_INI' => array('l' => 200, 't' => SQLT_CHR),
            'I_NOV_DATA_FIM' => array('l' => 200, 't' => SQLT_CHR),
            'I_NOV_STATUS' => array('l' => 200, 't' => SQLT_CHR),
            'I_NOV_SELECIONADOS' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDExcluirNovidade($dados)
    {
        $alias = 'ExcluirNovidade';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_NOV_ID' => $dados['I_NOV_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_NOV_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDListaProdutosNovidade($dados)
    {
        $alias = 'ListaProdutosNovidade';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_NOV_ID' => $dados['I_NOV_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_NOV_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDListaPromocoesCadastrados($dados)
    {
        $alias = 'ListaPromocoes';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDListaProdutosEspeciaisAdm($dados)
    {
        $alias = 'ListaEspeciais';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDDadosPromocao($dados)
    {
        $alias = 'DadosPromocao';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_PRO_ID' => $dados['I_PRO_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRO_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDProdutoEspecial($dados)
    {
        $alias = 'ProdutoEspecial';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_PRO_ID' => $dados['I_PRO_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRO_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDCadastrarEditarPromocao($dados)
    {
        $alias = 'CadastrarEditarPromocao';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_PRO_ID' => $dados['I_PRO_ID'],
            'I_PRO_DESCRICAO' => $dados['I_PRO_DESCRICAO'],
            'I_PRO_TIPO' => $dados['I_PRO_TIPO'],
            'I_PRO_DATA_INI' => $dados['I_PRO_DATA_INI'],
            'I_PRO_DATA_FIM' => $dados['I_PRO_DATA_FIM'],
            'I_PRO_STATUS' => $dados['I_PRO_STATUS'],
            'I_PRO_SELECIONADOS' => $dados['I_PRO_SELECIONADOS']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRO_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRO_DESCRICAO' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRO_TIPO' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRO_DATA_INI' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRO_DATA_FIM' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRO_STATUS' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRO_SELECIONADOS' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDCadastrarEditarProdutoEsp($dados)
    {
        $alias = 'CadastrarEditarProdutoEsp';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_PRO_ID' => $dados['I_PRO_ID'],
            'I_PRO_DESCRICAO' => $dados['I_PRO_DESCRICAO'],
            'I_PRO_TIPO' => $dados['I_PRO_TIPO'],
            'I_PRO_DATA_INI' => $dados['I_PRO_DATA_INI'],
            'I_PRO_DATA_FIM' => $dados['I_PRO_DATA_FIM'],
            'I_PRO_STATUS' => $dados['I_PRO_STATUS'],
            'I_PRO_SELECIONADOS' => $dados['I_PRO_SELECIONADOS']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRO_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRO_DESCRICAO' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRO_TIPO' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRO_DATA_INI' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRO_DATA_FIM' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRO_STATUS' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRO_SELECIONADOS' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDExcluirPromocao($dados)
    {
        $alias = 'ExcluirPromocao';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_PRO_ID' => $dados['I_PRO_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRO_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDExcluirProdutoEsp($dados)
    {
        $alias = 'ExcluirProdutoEsp';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_PRO_ID' => $dados['I_PRO_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRO_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDListaProdutosPromocao($dados)
    {
        $alias = 'ListaProdutosPromocao';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_PRO_ID' => $dados['I_PRO_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRO_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDListaProdutosEspeciaisCadastrados($dados)
    {
        $alias = 'ListaProdutosEspeciais';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_PRO_ID' => $dados['I_PRO_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRO_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDListaEmailsNewsletter($dados)
    {
        $alias = 'ListaEmailsNewsletter';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDListaContatoWhats($dados)
    {
        $alias = 'ListaContatoWhats';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDListaPerfisCadastrados($dados)
    {
        $alias = 'ListaPerfisCadastrados';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDDadosPerfis($dados)
    {
        $alias = 'DadosPerfis';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_PER_ID' => $dados['I_PER_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_PER_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDCadastrarEditarPerfis($dados)
    {
        $alias = 'CadastrarEditarPerfis';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_PER_ID' => $dados['I_PER_ID'],
            'I_PER_SIGLA' => $dados['I_PER_SIGLA'],
            'I_PER_DESC' => $dados['I_PER_DESC'],
            'I_PER_PERMISSIONS' => $dados['I_PER_PERMISSIONS']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_PER_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_PER_SIGLA' => array('l' => 200, 't' => SQLT_CHR),
            'I_PER_DESC' => array('l' => 200, 't' => SQLT_CHR),
            'I_PER_PERMISSIONS' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDExcluirPerfis($dados)
    {
        $alias = 'ExcluirPerfis';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_PER_ID' => $dados['I_PER_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_PER_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDListaUsuariosCadastrados($dados)
    {
        $alias = 'ListaUsuariosCadastrados';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDDadosUsuarios($dados)
    {
        $alias = 'DadosUsuarios';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_US_ID' => $dados['I_US_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_US_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDCadastrarEditarUsuarios($dados)
    {
        $alias = 'CadastrarEditarUsuarios';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_US_ID' => $dados['I_US_ID'],
            'I_PER_ID' => $dados['I_PER_ID'],
            'I_US_LOGIN' => $dados['I_US_LOGIN'],
            'I_US_PASS' => $dados['I_US_PASS'],
            'I_US_STATUS' => $dados['I_US_STATUS'],
            'I_US_IMAGE' => $dados['I_US_IMAGE'],
            'I_NOME_USER' => $dados['I_NOME_USER'],
            'I_EMAIL_USER' => $dados['I_EMAIL_USER'],
            'I_GENERO_USER' => $dados['I_GENERO_USER'],
            'I_TEL_USER' => $dados['I_TEL_USER'],
            'I_PASTA_USER' => $dados['I_PASTA_USER']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_US_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_PER_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_US_LOGIN' => array('l' => 200, 't' => SQLT_CHR),
            'I_US_PASS' => array('l' => 200, 't' => SQLT_CHR),
            'I_US_STATUS' => array('l' => 200, 't' => SQLT_CHR),
            'I_US_IMAGE' => array('l' => 200, 't' => SQLT_CHR),
            'I_NOME_USER' => array('l' => 200, 't' => SQLT_CHR),
            'I_EMAIL_USER' => array('l' => 200, 't' => SQLT_CHR),
            'I_GENERO_USER' => array('l' => 200, 't' => SQLT_CHR),
            'I_TEL_USER' => array('l' => 200, 't' => SQLT_CHR),
            'I_PASTA_USER' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDExcluirUsuarios($dados)
    {
        $alias = 'ExcluirUsuarios';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_US_ID' => $dados['I_US_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_US_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDUsuarioLogado($dados)
    {
        $alias = 'UsuarioLogado';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDUsuarioPorID($dados)
    {
        $alias = 'UsuarioPorID';

        $parametros = [
            'I_US_ID' => $dados['I_US_ID']
        ];

        $tipos = [
            'I_US_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos);
    }

    function servicoSDConfiguracoesADM($dados)
    {
        $alias = 'ConfiguracoesADM';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR)
        ];
        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDConfiguracoesNotoken()
    {
        $alias = 'ConfigNoToken';

        $parametros = [];

        $tipos = [];
        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDSalvarInfoContato($dados)
    {
        $alias = 'SalvarInfoContato';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_ID' => $dados['I_ID'],
            'I_ENDERECO' => $dados['I_ENDERECO'],
            'I_BAIRRO' => $dados['I_BAIRRO'],
            'I_NUMERO' => $dados['I_NUMERO'],
            'I_CIDADE' => $dados['I_CIDADE'],
            'I_UF' => $dados['I_UF'],
            'I_CEP' => $dados['I_CEP'],
            'I_COMPLEMENTO' => $dados['I_COMPLEMENTO'],
            'I_TELEFONE' => $dados['I_TELEFONE'],
            'I_EMAIL_TECNICO' => $dados['I_EMAIL_TECNICO']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_ENDERECO' => array('l' => 200, 't' => SQLT_CHR),
            'I_BAIRRO' => array('l' => 200, 't' => SQLT_CHR),
            'I_NUMERO' => array('l' => 200, 't' => SQLT_CHR),
            'I_CIDADE' => array('l' => 200, 't' => SQLT_CHR),
            'I_UF' => array('l' => 200, 't' => SQLT_CHR),
            'I_CEP' => array('l' => 200, 't' => SQLT_CHR),
            'I_COMPLEMENTO' => array('l' => 200, 't' => SQLT_CHR),
            'I_TELEFONE' => array('l' => 200, 't' => SQLT_CHR),
            'I_EMAIL_TECNICO' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDSalvarInfoSMTP($dados)
    {
        $alias = 'SalvarInfoSMTP';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_ID' => $dados['I_ID'],
            'I_SMTP_HOST' => $dados['I_SMTP_HOST'],
            'I_SMTP_PORTA' => $dados['I_SMTP_PORTA'],
            'I_SMTP_USER' => $dados['I_SMTP_USER'],
            'I_SMTP_SENHA' => $dados['I_SMTP_SENHA'],
            'I_MAIL_CONTATO_DEFAULT' => $dados['I_MAIL_CONTATO_DEFAULT'],
            'I_NOME_CONTATO_DEFAULT' => $dados['I_NOME_CONTATO_DEFAULT']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_SMTP_HOST' => array('l' => 200, 't' => SQLT_CHR),
            'I_SMTP_PORTA' => array('l' => 200, 't' => SQLT_CHR),
            'I_SMTP_USER' => array('l' => 200, 't' => SQLT_CHR),
            'I_SMTP_SENHA' => array('l' => 200, 't' => SQLT_CHR),
            'I_MAIL_CONTATO_DEFAULT' => array('l' => 200, 't' => SQLT_CHR),
            'I_NOME_CONTATO_DEFAULT' => array('l' => 200, 't' => SQLT_CHR),
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDConfigEnvioEmail($dados)
    {
        $alias = 'ConfigEnvioEmail';

        $parametros = [];

        $tipos = [];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDUsuarioPorEmail($dados)
    {
        $alias = 'UsuarioPorEmail';

        $parametros = [
            'I_MAIL_ID' => $dados['I_MAIL_ID']
        ];

        $tipos = [
            'I_MAIL_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos);
    }

    function servicoSDSalvarInformacoes($dados)
    {
        $alias = 'SalvarInformacoes';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_ID' => $dados['I_ID'],
            'I_INFO' => $dados['I_INFO'],
            'I_TIPO' => $dados['I_TIPO']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_INFO' => array('l' => 4000, 't' => SQLT_CHR),
            'I_TIPO' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDSalvarIntegracoes($dados)
    {
        $alias = 'SalvarIntegracoes';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_ID' => $dados['I_ID'],
            'I_IP_INTEGRACAO' => $dados['I_IP_INTEGRACAO'],
            'I_EMAIL_NOTIFICACAO' => $dados['I_EMAIL_NOTIFICACAO'],
            'I_UNI_MED_PESO' => $dados['I_UNI_MED_PESO'],
            'I_UNI_MED_TAM' => $dados['I_UNI_MED_TAM'],
            'I_CORREIO_USUARIO' => $dados['I_CORREIO_USUARIO'],
            'I_CORREIO_SENHA' => $dados['I_CORREIO_SENHA'],
            'I_TRACKING_ID' => $dados['I_TRACKING_ID'],
            'I_SITE_ID' => $dados['I_SITE_ID'],
            'I_CHAVE_CAPTCHA' => $dados['I_CHAVE_CAPTCHA'],
            'I_FACEBOOK_ID' => $dados['I_FACEBOOK_ID'],
            'I_APP_SECRET_ID' => $dados['I_APP_SECRET_ID'],
            'I_CIELO_MECHANT_ID' => $dados['I_CIELO_MECHANT_ID'],
            'I_CIELO_MECHANT_KEY' => $dados['I_CIELO_MECHANT_KEY'],
            'I_CIELO_CLIENT_ID' => $dados['I_CIELO_CLIENT_ID'],
            'I_PAGSEG_EMAIL' => $dados['I_PAGSEG_EMAIL'],
            'I_PAGSEG_TIPO_ENVIO' => $dados['I_PAGSEG_TIPO_ENVIO'],
            'I_PAGSEG_SAND_TOKEN' => $dados['I_PAGSEG_SAND_TOKEN'],
            'I_PAGSEG_SAND_APP_ID' => $dados['I_PAGSEG_SAND_APP_ID'],
            'I_PAGSEG_SAND_APP_KEY' => $dados['I_PAGSEG_SAND_APP_KEY'],
            'I_PAGSEG_PRD_TOKEN' => $dados['I_PAGSEG_PRD_TOKEN'],
            'I_PAGSEG_PRD_APP_ID' => $dados['I_PAGSEG_PRD_APP_ID'],
            'I_PAGSEG_PRD_APP_KEY' => $dados['I_PAGSEG_PRD_APP_KEY'],
            'I_TAG_MANAGER_ID' => $dados['I_TAG_MANAGER_ID'],
            'I_GOOGLE_USUARIO' => $dados['I_GOOGLE_USUARIO'],
            'I_GOOGLE_SENHA' => $dados['I_GOOGLE_SENHA']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_IP_INTEGRACAO' => array('l' => 200, 't' => SQLT_CHR),
            'I_UNI_MED_PESO' => array('l' => 200, 't' => SQLT_CHR),
            'I_UNI_MED_TAM' => array('l' => 200, 't' => SQLT_CHR),
            'I_CORREIO_USUARIO' => array('l' => 200, 't' => SQLT_CHR),
            'I_CORREIO_SENHA' => array('l' => 200, 't' => SQLT_CHR),
            'I_TRACKING_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_SITE_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_CHAVE_CAPTCHA' => array('l' => 200, 't' => SQLT_CHR),
            'I_FACEBOOK_ID' => array('l' => 400, 't' => SQLT_CHR),
            'I_APP_SECRET_ID' => array('l' => 400, 't' => SQLT_CHR),
            'I_CIELO_MECHANT_ID' => array('l' => 400, 't' => SQLT_CHR),
            'I_CIELO_MECHANT_KEY' => array('l' => 400, 't' => SQLT_CHR),
            'I_CIELO_CLIENT_ID' => array('l' => 400, 't' => SQLT_CHR),
            'I_PAGSEG_EMAIL' => array('l' => 400, 't' => SQLT_CHR),
            'I_PAGSEG_TIPO_ENVIO' => array('l' => 400, 't' => SQLT_CHR),
            'I_PAGSEG_SAND_TOKEN' => array('l' => 400, 't' => SQLT_CHR),
            'I_PAGSEG_SAND_APP_ID' => array('l' => 400, 't' => SQLT_CHR),
            'I_PAGSEG_SAND_APP_KEY' => array('l' => 400, 't' => SQLT_CHR),
            'I_PAGSEG_PRD_TOKEN' => array('l' => 400, 't' => SQLT_CHR),
            'I_PAGSEG_PRD_APP_ID' => array('l' => 400, 't' => SQLT_CHR),
            'I_PAGSEG_PRD_APP_KEY' => array('l' => 400, 't' => SQLT_CHR),
            'I_TAG_MANAGER_ID' => array('l' => 400, 't' => SQLT_CHR),
            'I_GOOGLE_USUARIO' => array('l' => 400, 't' => SQLT_CHR),
            'I_GOOGLE_SENHA' => array('l' => 400, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDSalvarImagens($dados)
    {
        $alias = 'SalvarImagens';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_ID' => $dados['I_ID'],
            'I_LOGOTIPO' => $dados['I_LOGOTIPO'],
            'I_FAVICON' => $dados['I_FAVICON']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_LOGOTIPO' => array('l' => 200, 't' => SQLT_CHR),
            'I_FAVICON' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDSalvarVideos($dados)
    {
        $alias = 'SalvarVideos';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_ID' => $dados['I_ID'],
            'I_TIPO_VIDEO' => $dados['I_TIPO_VIDEO'],
            'I_VIDEO_BOASVINDAS' => $dados['I_VIDEO_BOASVINDAS']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_TIPO_VIDEO' => array('l' => 200, 't' => SQLT_CHR),
            'I_VIDEO_BOASVINDAS' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDCadastrarCategoriaImportacao($dados)
    {
        $alias = 'CadastrarCategoriaImportacao';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_CAT_NOME' => $dados['I_CAT_NOME'],
            'I_CAT_STATUS' => $dados['I_CAT_STATUS'],
            'I_CAT_PAI' => $dados['I_CAT_PAI']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_CAT_NOME' => array('l' => 200, 't' => SQLT_CHR),
            'I_CAT_STATUS' => array('l' => 200, 't' => SQLT_CHR),
            'I_CAT_PAI' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDListaMeiosCadastrados($dados)
    {
        $alias = 'ListaMeiosPagamentos';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDDadosMeioPagamento($dados)
    {
        $alias = 'DadosMeioPagamento';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_MEIO_ID' => $dados['I_MEIO_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_MEIO_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDCadastrarEditarMeioPagamento($dados)
    {
        $alias = 'CadastrarEditarMeioPagamento';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_MEIO_ID' => $dados['I_MEIO_ID'],
            'I_NOME_MEIO' => $dados['I_NOME_MEIO'],
            'I_DESCRICAO_MEIO' => $dados['I_DESCRICAO_MEIO'],
            'I_MP_STATUS' => $dados['I_MP_STATUS'],
            'I_IMAGEM' => $dados['I_IMAGEM']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_MEIO_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_NOME_MEIO' => array('l' => 200, 't' => SQLT_CHR),
            'I_DESCRICAO_MEIO' => array('l' => 200, 't' => SQLT_CHR),
            'I_MP_STATUS' => array('l' => 200, 't' => SQLT_CHR),
            'I_IMAGEM' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDExcluirMeioPagamento($dados)
    {
        $alias = 'ExcluirMeioPagamento';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_MEIO_ID' => $dados['I_MEIO_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_MEIO_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDClientePorEmail($dados)
    {
        $alias = 'ClientePorEmail';

        $parametros = [
            'I_MAIL_ID' => $dados['I_MAIL_ID']
        ];

        $tipos = [
            'I_MAIL_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos);
    }

    function servicoSDTrocarSenha($dados)
    {
        $alias = 'TrocarSenha';

        $parametros = [
            'I_US_ID' => $dados['I_US_ID'],
            'I_US_LOGIN' => $dados['I_US_LOGIN'],
            'I_US_SENHA' => $dados['I_US_SENHA'],
            'I_US_STATUS' => $dados['I_US_STATUS']
        ];

        $tipos = [
            'I_US_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_US_LOGIN' => array('l' => 200, 't' => SQLT_CHR),
            'I_US_SENHA' => array('l' => 200, 't' => SQLT_CHR),
            'I_US_STATUS' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDListaModaisCadastrados($dados)
    {
        $alias = 'ListaModais';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDDadosModal($dados)
    {
        $alias = 'DadosModal';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_MOD_ID' => $dados['I_MOD_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_MOD_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDCadastrarEditarModal($dados)
    {
        $alias = 'CadastrarEditarModal';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_MOD_ID' => $dados['I_MOD_ID'],
            'I_MOD_DESCRICAO' => $dados['I_MOD_DESCRICAO'],
            'I_MOD_LINK' => $dados['I_MOD_LINK'],
            'I_MOD_DATA_INI' => $dados['I_MOD_DATA_INI'],
            'I_MOD_DATA_FIM' => $dados['I_MOD_DATA_FIM'],
            'I_MOD_STATUS' => $dados['I_MOD_STATUS'],
            'I_MOD_IMAGEM' => $dados['I_MOD_IMAGEM'],
            'I_MOD_PASTA_IMAGENS' => $dados['I_MOD_PASTA_IMAGENS']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_MOD_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_MOD_DESCRICAO' => array('l' => 200, 't' => SQLT_CHR),
            'I_MOD_LINK' => array('l' => 200, 't' => SQLT_CHR),
            'I_MOD_DATA_INI' => array('l' => 200, 't' => SQLT_CHR),
            'I_MOD_DATA_FIM' => array('l' => 200, 't' => SQLT_CHR),
            'I_MOD_STATUS' => array('l' => 200, 't' => SQLT_CHR),
            'I_MOD_IMAGEM' => array('l' => 200, 't' => SQLT_CHR),
            'I_MOD_PASTA_IMAGENS' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDExcluirModal($dados)
    {
        $alias = 'ExcluirModal';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_MOD_ID' => $dados['I_MOD_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_MOD_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDListaPedidosCadastrados($dados)
    {
        $alias = 'ListaPedidosCadastrados';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDListaProdutosCadastrados($dados)
    {
        $alias = 'ListaProdutosCadastrados';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDListaDetalhePedido($dados)
    {
        $alias = 'DetalhePedido';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_PED_ID' => $dados['I_PED_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_PED_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDCancelaPedido($dados)
    {
        $alias = 'CancelaPedido';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_PED_ID' => $dados['I_PED_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_PED_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDListaRelatorios($dados)
    {
        $alias = 'ListaRelatorio';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_TIPO' => $dados['I_TIPO']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_TIPO' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDListaMeiosEntregaCadastrados($dados)
    {
        $alias = 'ListaMeiosEntrega';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDDadosMeioEntrega($dados)
    {
        $alias = 'DadosMeioEntrega';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_MEIO_ID' => $dados['I_MEIO_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_MEIO_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDListaCidadesAbrangencia($dados)
    {
        $alias = 'ListaCidadesAbrangencia';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_MEIO_ID' => $dados['I_MEIO_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_MEIO_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDCadastrarEditarMeioEntrega($dados)
    {
        $alias = 'CadastrarEditarMeioEntrega';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_MEIO_ID' => $dados['I_MEIO_ID'],
            'I_NOME_MEIO' => $dados['I_NOME_MEIO'],
            'I_DESCRICAO_MEIO' => $dados['I_DESCRICAO_MEIO'],
            'I_CODIGO_MEIO' => $dados['I_CODIGO_MEIO'],
            'I_EMPRESA' => $dados['I_EMPRESA'],
            'I_RESTRICAO' => $dados['I_RESTRICAO'],
            'I_VALOR_COBRADO' => $dados['I_VALOR_COBRADO'],
            'I_PRAZO_MAXIMO' => $dados['I_PRAZO_MAXIMO'],
            'I_ME_STATUS' => $dados['I_ME_STATUS'],
            'I_CALC_FRETE' => $dados['I_CALC_FRETE'],
            'I_IMAGEM' => $dados['I_IMAGEM']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_MEIO_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_NOME_MEIO' => array('l' => 200, 't' => SQLT_CHR),
            'I_DESCRICAO_MEIO' => array('l' => 200, 't' => SQLT_CHR),
            'I_CODIGO_MEIO' => array('l' => 200, 't' => SQLT_CHR),
            'I_EMPRESA' => array('l' => 200, 't' => SQLT_CHR),
            'I_RESTRICAO' => array('l' => 200, 't' => SQLT_CHR),
            'I_VALOR_COBRADO' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRAZO_MAXIMO' => array('l' => 200, 't' => SQLT_CHR),
            'I_ME_STATUS' => array('l' => 200, 't' => SQLT_CHR),
            'I_CALC_FRETE' => array('l' => 200, 't' => SQLT_CHR),
            'I_IMAGEM' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDCadastrarAbrangencia($dados)
    {
        $alias = 'CadastrarAbrangencia';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_MEIO_ID' => $dados['I_MEIO_ID'],
            'I_CIDADE' => $dados['I_CIDADE'],
            'I_ESTADO' => $dados['I_ESTADO']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_MEIO_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_CIDADE' => array('l' => 200, 't' => SQLT_CHR),
            'I_ESTADO' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDExcluirMeioEntrega($dados)
    {
        $alias = 'ExcluirMeioEntrega';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_MEIO_ID' => $dados['I_MEIO_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_MEIO_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDSepararDoEstoque($dados)
    {
        $alias = 'SepararDoEstoque';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_PED_ID' => $dados['I_PED_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_PED_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDSepararEnvioCliente($dados)
    {
        $alias = 'SepararEnvioCliente';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_PED_ID' => $dados['I_PED_ID'],
            'I_LOCALIZADOR' => $dados['I_LOCALIZADOR'],
            'I_OBSERVACAO' => $dados['I_OBSERVACAO']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_PED_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_LOCALIZADOR' => array('l' => 200, 't' => SQLT_CHR),
            'I_OBSERVACAO' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDReverterPedido($dados)
    {
        $alias = 'ReverterPedido';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_PED_ID' => $dados['I_PED_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_PED_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDItensPedido($dados)
    {
        $alias = 'ItensPedido';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_PED_ID' => $dados['I_PED_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_PED_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDCadastrarEditarCliente($dados)
    {
        $alias = 'CadastrarEditarCliente';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_CLI_ID' => $dados['I_CLI_ID'],
            'I_NOME_COMPLETO' => $dados['I_NOME_COMPLETO'],
            'I_TEL_RESID' => $dados['I_TEL_RESID'],
            'I_TEL_COM' => $dados['I_TEL_COM'],
            'I_TEL_CEL' => $dados['I_TEL_CEL'],
            'I_TIPO_DOC' => $dados['I_TIPO_DOC'],
            'I_NUM_DOC' => $dados['I_NUM_DOC'],
            'I_GENERO' => $dados['I_GENERO'],
            'I_DT_NASC' => $dados['I_DT_NASC'],
            'I_EMAIL' => $dados['I_EMAIL'],
            'I_OCUPACAO' => $dados['I_OCUPACAO']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_CLI_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_NOME_COMPLETO' => array('l' => 200, 't' => SQLT_CHR),
            'I_TEL_RESID' => array('l' => 200, 't' => SQLT_CHR),
            'I_TEL_COM' => array('l' => 200, 't' => SQLT_CHR),
            'I_TEL_CEL' => array('l' => 200, 't' => SQLT_CHR),
            'I_TIPO_DOC' => array('l' => 200, 't' => SQLT_CHR),
            'I_NUM_DOC' => array('l' => 200, 't' => SQLT_CHR),
            'I_GENERO' => array('l' => 200, 't' => SQLT_CHR),
            'I_DT_NASC' => array('l' => 200, 't' => SQLT_CHR),
            'I_EMAIL' => array('l' => 200, 't' => SQLT_CHR),
            'I_OCUPACAO' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDEditarEnderecoCliente($dados)
    {
        $alias = 'EditarEnderecoCliente';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_CLI_ID' => $dados['I_CLI_ID'],
            'I_END_ID' => $dados['I_END_ID'],
            'I_END_CEP' => $dados['I_END_CEP'],
            'I_END_LOGRADOURO' => $dados['I_END_LOGRADOURO'],
            'I_END_BAIRRO' => $dados['I_END_BAIRRO'],
            'I_END_CIDADE' => $dados['I_END_CIDADE'],
            'I_END_NUMERO' => $dados['I_END_NUMERO'],
            'I_END_UF' => $dados['I_END_UF'],
            'I_END_PAIS' => $dados['I_END_PAIS'],
            'I_END_COMPLEMENTO' => $dados['I_END_COMPLEMENTO'],
            'I_END_TIPO' => $dados['I_END_TIPO'],
            'I_END_ENVIAR' => $dados['I_END_ENVIAR'],
            'I_END_STATUS' => $dados['I_END_STATUS']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_CLI_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_END_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_END_CEP' => array('l' => 200, 't' => SQLT_CHR),
            'I_END_LOGRADOURO' => array('l' => 200, 't' => SQLT_CHR),
            'I_END_BAIRRO' => array('l' => 200, 't' => SQLT_CHR),
            'I_END_CIDADE' => array('l' => 200, 't' => SQLT_CHR),
            'I_END_NUMERO' => array('l' => 200, 't' => SQLT_CHR),
            'I_END_UF' => array('l' => 200, 't' => SQLT_CHR),
            'I_END_PAIS' => array('l' => 200, 't' => SQLT_CHR),
            'I_END_COMPLEMENTO' => array('l' => 200, 't' => SQLT_CHR),
            'I_END_TIPO' => array('l' => 200, 't' => SQLT_CHR),
            'I_END_ENVIAR' => array('l' => 200, 't' => SQLT_CHR),
            'I_END_STATUS' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDListaClientes($dados)
    {
        $alias = 'ListaClientes';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_ORDER' => $dados['I_ORDER'],
            'I_DIR' => $dados['I_DIR'],
            'I_START' => $dados['I_START'],
            'I_LENGTH' => $dados['I_LENGTH'],
            'I_NOME' => $dados['I_NOME'],
            'I_DATA' => $dados['I_DATA'],
            'I_EMAIL' => $dados['I_EMAIL']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_ORDER' => array('l' => 200, 't' => SQLT_CHR),
            'I_DIR' => array('l' => 200, 't' => SQLT_CHR),
            'I_START' => array('l' => 200, 't' => SQLT_CHR),
            'I_LENGTH' => array('l' => 200, 't' => SQLT_CHR),
            'I_NOME' => array('l' => 200, 't' => SQLT_CHR),
            'I_DATA' => array('l' => 200, 't' => SQLT_CHR),
            'I_EMAIL' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDDadosClientes($dados)
    {
        $alias = 'DadosClientes';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_CLI_ID' => $dados['I_CLI_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_CLI_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDDadosEnderecoClientes($dados)
    {
        $alias = 'DadosEndClientes';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_CLI_ID' => $dados['I_CLI_ID'],
            'I_END_ID' => $dados['I_END_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_CLI_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_END_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDDadosCompraClientes($dados)
    {
        $alias = 'DadosCompraCliente';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_CLI_ID' => $dados['I_CLI_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_CLI_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDEnderecosClientes($dados)
    {
        $alias = 'EnderecosClientes';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_CLI_ID' => $dados['I_CLI_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_CLI_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDExcluirClientes($dados)
    {
        $alias = 'ExcluirClientes';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_US_ID' => $dados['I_US_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_US_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDExcluirEnderecoClientes($dados)
    {
        $alias = 'ExcluirEnderecoClientes';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_CLI_ID' => $dados['I_CLI_ID'],
            'I_END_ID' => $dados['I_END_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_CLI_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_END_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDResetarSenhaClientes($dados)
    {
        $alias = 'ResetarSenhaClientes';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_ClI_ID' => $dados['I_ClI_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_ClI_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDDadosHome($dados)
    {
        $alias = 'DadosHome';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDAtivarInativarUsuario($dados)
    {
        $alias = 'AtivarInativarUsuario';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_US_ID' => $dados['I_US_ID'],
            'I_US_STATUS' => $dados['I_US_STATUS']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_US_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_US_STATUS' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDAtivarInativarCliente($dados)
    {
        $alias = 'AtivarInativarCliente';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_CLI_ID' => $dados['I_CLI_ID'],
            'I_CLI_STATUS' => $dados['I_CLI_STATUS']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_CLI_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_CLI_STATUS' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDAtivarInativarEnderecoCliente($dados)
    {
        $alias = 'AtivarInativarEndCliente';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_CLI_ID' => $dados['I_CLI_ID'],
            'I_END_ID' => $dados['I_END_ID'],
            'I_END_STATUS' => $dados['I_END_STATUS']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_CLI_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_END_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_END_STATUS' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDListaLoginsClientes($dados)
    {
        $alias = 'ListaLoginsUsuarios';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_PER_ID' => 3
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_PER_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDListaLoginsUsuariosSistema($dados)
    {
        $alias = 'ListaLoginsUsuarios';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_PER_ID' => 1
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_PER_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDListaUltimasBuscas($dados)
    {
        $alias = 'ListaUltimasBuscas';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDListaAcessosCidades($dados)
    {
        $alias = 'ListaAcessosCidades';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDVerificaEmailClienteExiste($dados)
    {
        $alias = 'VerificaEmailClienteExiste';

        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_EMAIL' => $dados['I_EMAIL'],
            'I_ClI_ID' => $dados['I_ClI_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_EMAIL' => array('l' => 200, 't' => SQLT_CHR),
            'I_ClI_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDVerificaCPFClienteExiste($dados)
    {
        $alias = 'VerificaCPFClienteExiste';

        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_CPF' => $dados['I_CPF'],
            'I_ClI_ID' => $dados['I_ClI_ID']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_CPF' => array('l' => 200, 't' => SQLT_CHR),
            'I_ClI_ID' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDInativaTokensExpirados($dados)
    {
        $alias = 'InativaTokensExpirados';

        $parametros = [];

        $tipos = [];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDInfoProdutosPorSKU($dados)
    {
        $alias = 'InfoProdutosPorSKU';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_PRD_SKU' => $dados['I_PRD_SKU'],
            'I_IMAGEM' => $dados['I_IMAGEM']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_SKU' => array('l' => 200, 't' => SQLT_CHR),
            'I_IMAGEM' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDDadosProdutoPorSKU($dados)
    {
        $alias = 'DadosProdutoPorSku';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_PRD_SKU' => $dados['I_PRD_SKU']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_SKU' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDLerNotificacoes($dados)
    {
        $alias = 'LerNotificacoes';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDNotificacaoLida($dados)
    {
        $alias = 'NotificacaoLida';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_OP' => $dados['I_OP'],
            'I_TIPO' => $dados['I_TIPO']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR),
            'I_OP' => array('l' => 200, 't' => SQLT_CHR),
            'I_TIPO' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDListaPedidosCron()
    {
        $alias = 'ListaPedidosCron';

        $parametros = [];

        $tipos = [];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDAtualizaAPIPedidoCron($dados)
    {
        $alias = 'AtualizaAPIPedidoCron';

        $parametros = [
            'I_PED_ID' => $dados['I_PED_ID'],
            'I_PED_STATUS' => $dados['I_PED_STATUS'],
            'I_PED_DATE' => $dados['I_PED_DATE']
        ];

        $tipos = [
            'I_PED_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_PED_STATUS' => array('l' => 200, 't' => SQLT_CHR),
            'I_PED_DATE' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    /* teste de importação e Cron*/
    function servicoSDCronListaProdutos($dados)
    {
        ini_set('max_execution_time', 3000); //300 seconds = 5 minutes

        $alias = 'CronListaProdutos';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDCronUpdateProduto($dados)
    {
        $alias = 'CronUpdateProduto';

        $parametros = [
            'I_PRD_ID' => $dados['I_PRD_ID'],
            'I_PASTA_IMAGENS' => $dados['I_PASTA_IMAGENS'],
            'I_PRD_IMAGEM_PRINCIPAL' => $dados['I_PRD_IMAGEM_PRINCIPAL']
        ];

        $tipos = [
            'I_PRD_ID' => array('l' => 200, 't' => SQLT_CHR),
            'I_PASTA_IMAGENS' => array('l' => 200, 't' => SQLT_CHR),
            'I_PRD_IMAGEM_PRINCIPAL' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDTesteImportacaoImagens($dados)
    {
        $alias = 'TesteImportacaoImagens';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }

    function servicoSDTesteSelecionarImagens($dados)
    {
        $alias = 'TesteSelecionarImagens';

        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN']
        ];

        $tipos = [
            'I_TOKEN' => array('l' => 200, 't' => SQLT_CHR)
        ];

        return self::consultarSQLBase($alias, $parametros, $tipos, PROCEDURE);
    }
}
