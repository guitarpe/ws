<?php

class Funcoes
{

    function recebe_chamada($metodo, $sistema, $dados = array())
    {

        $classe_selecionada = '';

        switch ($sistema) {
            case 'SantosDummont':
                $classe_selecionada = 'SantosDummont';
                require_once 'SantosDummont.php';
                break;
            default:
                $classe_selecionada = '';
                break;
        }

        if (!empty($classe_selecionada)) {
            $class = new $classe_selecionada();
            $retorno_metodo = $class->$metodo($dados);
        } else {
            return array('erro' => true, 'message' => 'Sistema não Validado', 'list' => null);
        }

        return $retorno_metodo;
    }
}

?>